# Viktor's new dotfiles

This repo contains two things:

- `nix`- nix files for my nix setup
- `stow`- manual dotfiles to be used with stow relative \$HOME.

## nix

So I'm doing nixos. Nix config files resolves imports relative to the actual physical files so I can simply softlink into ONE file and be done:

    sudo ln -s ~/dotfiles/nix/machines/marfa/configuration.nix /etc/nixos/configuration.nix
    sudo nixos-rebuild switch

hardware-configuration.nix is imported with absolut path.

## Dotfiles with stow

I'm ditching my previous dotfiles repo which used git `--bare` and `--work-tree=$HOME` to simply add files all over the place. I had two problems with that approach:

- Hard to manage different machines that might locate config files differently, say vscode settings for a Mac and nixos.
- Hard to get an overview without a physical worktree. The bare thing just made it feel very global and nasty. Did some accidental adds sometimes.

So I'm instead going to use gnu stow and arrange all my configs with a folder per app/thing and stow separately. For files that might have different location I can symlink internal to the git repo.

Setup symlinks with:

    stow -d ~/dev/dotfiles/stow -t ~ -R -v git
    stow -d ~/dev/dotfiles/stow -t ~ -R -v fish

and such.

## Brew for OSX

I'm using brew bundle and a Brewfile for OSX

To generate a brewfile from all existing installations

    brew bundle dump --all

or

    brew bundle dump --all --force

To use, just:

### Longer explaination

I started to get into the idea of doing _everything_ with .nix. I tried out home-manager and got into things like:

```
home-manager.users.viktor = {

    programs = {
      git = {
        enable = true;
        userName = "Viktor Hedefalk";
        userEmail = "hedefalk@gmail.com";
        #extraConfig = { credential = { helper = "store"; }; };
      };

      fish = {
        enable = true;
      };
    };

    # Manual config files
    home.file = {
      ".config/kxkbrc".source = ./dotfiles/kxkbrc; # KDE keyboard layout
      ".config/kcminputrc".source = ./dotfiles/kcminputrc; # KDE mouse and keyboard settings
    };

  };
};
```

So here there are some dedicated common settings like git setup that nixos "handles" for me. But all it really does is put that config into the right files. And for a lot of stuff there simply isn't dedicated nix way of doing it. So I'm gonna get into some manual config files anyway. So there I tried those `home.file` things of home-manager. They work by simply copying the right hand side to the left hand side on build. But for a desktop env, this is just not workable for things like KDE settings. I'd have to iterate over my settings, then see what changed, and then put it back into the source file. I just don't see a good reason to do it that way.

With the stow approach, it's just symlinks and I can use KDE to make settings for KDE. Then I'll just git diff and commit and honkey dorey. I'll add a tiny bit of statefulness for the sake of faster iteration and ease of use.

## New Hackintosh

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

    brew bundle --file brew/Brewfile_hack

    stow -t ~ git
    stow -t ~ fish
    stow -t ~ vscode

Setup Clipy
Setup spectacle

## nix-darwin + home-manager.

Home manager just as plugin to nix-darwin:

Make nix work first, then:

    ./result/bin/darwin-installer

These files need to be updated by the darwin-rebuild:

    sudo mv /etc/nix/nix.conf /etc/nix/nix.conf_BACK
    sudo mv /etc/shells /etc/shells_back

First time we have to point to the non-standard config location we stow at:

    stow -t ~ nix
    darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin-configuration.nix

From there on we can just

    darwin-rebuild switch

Change login shell of MacOS user.

In users ctrl-click user and "advanced options". Make login-shell reflect the one managed by nix-darwin, ie "/run/current-system/sw/bin/fish"

## Typical lorri usage:

.envrc:

    export GITHUB_TOKEN=$(pass github/fcg_repo_token)
    export ENV="dev"
    export SBT_OPTS="$SBT_OPTS -Dbloop.export-jar-classifiers=sources"
    export SBT_OPTS="$SBT_OPTS -Dsbt.sourcemode=false"
    export SBT_OPTS="$SBT_OPTS -Dwd=$PWD"
    export LC_ALL="en_US.UTF-8"
    export NIXPKGS_ALLOW_BROKEN=1
    export SAM_CLI_TELEMETRY=0
    eval "$(lorri direnv)"

shell.nix:

    {
      pkgs ? import <nixpkgs> { localSystem = "aarch64-darwin"; },
      pkgs_x86_64 ? import <nixpkgs> { localSystem = "x86_64-darwin"; },
    }:

    pkgs.mkShell {
      buildInputs = with pkgs; [
        # jdk11
        pkgs_x86_64.jdk11
        postgresql_13
        scala
        (sbt.override { jre = jdk11; }) # 11.0.9
        nodejs
        nodePackages.pnpm
        yarn
      ];
    }
