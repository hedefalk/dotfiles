{
  description = "Viktor's nix configurations";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nixpkgs-colima.url = "github:gdw2vs/nixpkgs/patch-1";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, nix-darwin, nixpkgs, nixpkgs-colima, home-manager }:
  let
    # configuration = { pkgs, ... }: {
    #   # Auto upgrade nix package and the daemon service.
    #   services.nix-daemon.enable = true;
    #   # Necessary for using flakes on this system.
    #   nix.settings.experimental-features = "nix-command flakes";
    #   programs.fish.enable = true;
    #   # Set Git commit hash for darwin-version.
    #   system.configurationRevision = self.rev or self.dirtyRev or null;
    #   # Used for backwards compatibility, please read the changelog before changing.
    #   # $ darwin-rebuild changelog
    #   system.stateVersion = 4;
    # };
    # mkDarwin = {
    #     extraDarwinModules ? {},
    # }:
    #     nix-darwin.lib.darwinSystem {
    #         system = "aarch64-darwin";
    #         specialArgs = {inherit self;};
    #         modules = [./darwin/common.nix] ++ extraDarwinModules;
    #     };
    # mkHm = {
    #     extraModules ? [],
    #     arch,
    # }:
    #     home-manager.lib.homeManagerConfiguration {
    #         pkgs = nixpkgs.legacyPackages.${arch};
    #         modules = extraModules;
    #         # modules = [ghostty.homeModules.default] ++ extraModules;
    #     };
    #
    # nixpkgs = {
    #     overlays = [
    #         # Cherry picked overlay
    #         (final: prev: {
    #             inherit (import nixpkgs-colima)
    #             colima;
    #         })
    #     ];
    # };
    overlay = final: prev: {
        inherit (nixpkgs-colima.legacyPackages.${prev.system})
            colima;

        # inherit (nixpkgs-foobarspam.legacyPackages.${prev.system})
        #     foobarspam-server foobarspam-cli foobarspam-lib;
        };
  in
  {
    darwinConfigurations = {
        Viktors-MacBook-Air = nix-darwin.lib.darwinSystem {
            system = "aarch64-darwin";
            # nixpkgs.overlays = [ overlay ];
            modules = [
              ({...}:
                {nixpkgs.overlays = [ overlay ];}
                )
              ./shared/common.nix
              ./shared/mac.nix
              ./hosts/air.nix
              home-manager.darwinModules.home-manager
                {
                    home-manager.useGlobalPkgs = true;
                    home-manager.useUserPackages = true;
                    home-manager.users.viktor = ./home/home.nix;
                }
            ];
        };
        Viktors-Mac-Studio = nix-darwin.lib.darwinSystem {
            system = "aarch64-darwin";
            modules = [
              ./shared/common.nix
              ./shared/mac.nix
              ./hosts/studio.nix
              home-manager.darwinModules.home-manager
                {
                    home-manager.useGlobalPkgs = true;
                    home-manager.useUserPackages = true;
                    home-manager.users.viktor = ./home/home.nix;
                }
            ];

        };
    };
  };
}
