# home.nix

{ ... }:

{
  home.stateVersion = "23.05"; # Please read the comment before changing.
  home.packages = [
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # programs.kubeswitch = {
    # enable = true;
    # commandName = "kubeswitch";
  # };
}
