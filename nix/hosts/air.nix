{ config, pkgs, nixpkgs, ... }:

{
  # imports = [ <home-manager/nix-darwin> ];
  nixpkgs.hostPlatform    = "aarch64-darwin";
  environment.systemPackages = with pkgs; [
    # monkeysphere  # not supported on ‘aarch64-darwin
    any-nix-shell
  ];


  programs = {
  };


  # home-manager.users.viktor = { pkgs, ... }: {
  #   home.packages = [ pkgs.atool pkgs.httpie ];
  #   programs.bash.enable = true;
  #   home.stateVersion = "24.05";
  # };



}
