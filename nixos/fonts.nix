{ pkgs, lib, ... }:

{
  fonts = {
    enableDefaultFonts = true;
    fonts = with pkgs; [ 
      fira-code
      jetbrains-mono
    ];
  };

 console = {
    font = lib.mkDefault "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";
    # font = lib.mkDefault "${pkgs.jetbrains-mono}/share/consolefonts/ter-u28n.psf.gz";
    # font = "JetBrainsMono-1.0.4";
    # defined twice because using xserver settings
    #keyMap = "sv-latin1";
  };
}
