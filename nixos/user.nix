{ pkgs, ... } :

  # imports = [ "${home-manager}/nixos" ];

  # Install packages to `/etc/profile` because upstream would default it in the future.
  # home-manager.useUserPackages = true;
{
  users.users.viktor = {
    isNormalUser = true;
    description = "Viktor Hedefalk";
    extraGroups = [ "wheel" "networkmanager" "audio" "video" ];
    openssh.authorizedKeys.keys = [
      # MBP
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDe8W+XLjtmGGxb3Z4R5Tb+B5Rinr+lqze2wKDntE2IOWUdh6yDwOQkjZLUmw7XvbtLPHI3gY9HYkHWILDfA26ef6RFzRSKZuykDUiPZNR9+OjrVNImjeTDZ0tWKDl5wuyqB3LL1utQkDSySksKYAdQ++sjoh4EJqe2yLJagQdV6k7e4cpNS8WVI7EG+75oEvsgZAejr4iMq9eZ/hPfIRXvLdO44yulp4z2UqnkBbL/60ri8ZnulgQdg9IpBBVPPYab7nuKAeEQmNz2/npBfAFQDo3GgpGZxKIdXzF0gFAGYailEq2EVj3SbZIJRTGa76kEFhS9xsP894EHAs3nNAv/ hedefalk@gmail.com"
    ];
    shell = pkgs.fish;
  };
}
