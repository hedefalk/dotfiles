{ config, pkgs, lib, ... }:

{
  # For Radeon 5700 XT
  boot.kernelPackages = pkgs.linuxPackages_latest;
  hardware.opengl = {
    enable = true;
    driSupport = true;
  };
  nixpkgs.config.allowUnfree = true;
  hardware.enableRedistributableFirmware = true;

  imports =
    [ 
      /etc/nixos/hardware-configuration.nix # generated hardware-config
      ../../fonts.nix
      ../../common-apps.nix
      ../../user.nix
      ./keyboard.nix
      ./screen.nix
      ./kde.nix
    ];

  # Enable the X11 windowing system.
  services = {
    xserver = {
      enable = true; # Actually enables the GUI
      desktopManager.plasma5.enable = true; # Enables a bare-bones Plasma desktop
      displayManager.sddm.enable = true; # Enables SDDM
      # libinput.enable = true; # Better touchpad support

    };
    
    openssh.enable = true;
  };

  programs.ssh.startAgent = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    #systemd-boot.enable = true;
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot";
    };
    grub = {
      devices = [ "nodev" ];
      efiSupport = true;
      enable = true;
      extraEntries = ''
        menuentry "Hackintosh BOOTx64" {
          insmod part_gpt
          insmod fat
          insmod search_fs_uuid
          insmod chain
          search --fs-uuid --set=root 67E3-17ED
          chainloader /EFI/BOOT/BOOTx64.efi
        }
        menuentry "Hackintosh OpenCore.efi" {
          insmod part_gpt
          insmod fat
          insmod search_fs_uuid
          insmod chain
          search --fs-uuid --set=root 67E3-17ED
          chainloader /EFI/OC/OpenCore.efi
        }
      '';
      version = 2;
      #useOSProber = true;
    };
  };

  networking.hostName = "marfa"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  networking.useDHCP = false;
  networking.interfaces.eno2.useDHCP = true;
  networking.interfaces.enp6s0.useDHCP = true;
  networking.interfaces.wlo1.useDHCP = true;

  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Stockholm";

  environment.systemPackages = with pkgs; [
    git
  ];

  # Enable sound.
  sound.enable = true;
  #hardware.pulseaudio.enable = true;

  # Auto gc and optimise
  nix.optimise.automatic = true;
  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 7d";

  system.stateVersion = "19.09";
}
