{ pkgs, ... }:

{ 
  environment.systemPackages = with pkgs; [
    libdbusmenu-glib
  ];
}
