{ pkgs, ... }:

{ 
  services.xserver = {
    autoRepeatDelay = 200;
    autoRepeatInterval = 20;
    layout = "se";
    xkbVariant = "mac";
    xkbModel = "microsoft4000";
  };

  console.useXkbConfig = true;
}
