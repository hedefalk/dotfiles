{ pkgs, ... }:

{
  imports = [
    ./vscode.nix
  ];

  environment.systemPackages = with pkgs; [
    kicad-with-packages3d
    stow
  ];
}
