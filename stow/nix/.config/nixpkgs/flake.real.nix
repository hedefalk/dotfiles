{
  description = "Viktor's MacOS configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-24.05-darwin";
    home-manager.url = "github:nix-community/home-manager/release-24.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, nix-darwin, home-manager, nixpkgs }: {
    darwinConfigurations = {

      "Viktors-Mac-Studio" = nix-darwin.lib.darwinSystem {
        modules = [ ./darwin-configuration.nix ];
      };

      "Viktors-MacBook-Air" = nix-darwin.lib.darwinSystem {
        modules = [ ./darwin-configuration.nix ];
      };

    };
  }
