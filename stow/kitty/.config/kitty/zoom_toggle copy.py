from kittens.tui.loop import debug
from kittens.tui.handler import result_handler


def main(args: List[str]) -> str:
    answer = input('Enter some text: ')
    debug('zoom toggle main!')
    pass


@result_handler(no_ui=True)
def handle_result(args, answer, target_window_id, boss):
    debug('zoom toggle!')
    tab = boss.active_tab
    if tab is not None:
        if tab.current_layout.name == 'stack':
            tab.last_used_layout()
        else:
            tab.goto_layout('stack')
