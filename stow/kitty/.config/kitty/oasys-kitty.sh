#!/usr/local/bin/fish

set sbt_id (kitty @ launch --keep-focus --copy-env --type tab --tab-title Oasys-backend --cwd /Users/viktor/dev/fcg/Oasys-backend)
set git_id (kitty @ launch --keep-focus --copy-env --match title:Oasys-backend --cwd /Users/viktor/dev/fcg/Oasys-backend)
set aux_id (kitty @ launch --keep-focus --copy-env --match title:Oasys-backend )

kitty @ send-text --match id:$sbt_id sbt "~test:compile" shell\n
kitty @ send-text --match id:$git_id git status\n




